## Prerekvizity

- PHP 7.2+
- PHP mbstring
- PHP exec
- PHP mysql
- Composer (pre PHP)
- MySQL, SQLite alebo hocičo iné pre čo existuje "Doctrine driver". Aplikácia zatiaľ nevyužíva žiadne SQL query, ktoré by boli špecifické pre kontrétny typ RDBS.
- Pokiaľ sa ide použivať databaza mysql na Ubuntu tak ju treba nainstalovať podľa návodu na nasledujúcej stránke: https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-18-04 

Install MySQL, PHP and required modules:
```
$ sudo apt install mysql-server php php-mysql php-mbstring php-zip
```
Komplentný návod je v súbore DEPENDENCIINSTALL.md v roote projektu

Update MySQL password:
```
$ sudo mysql
> UPDATE mysql.user SET plugin = 'mysql_native_password' WHERE User = '...';
> UPDATE mysql.user SET authentication_string = PASSWORD('...') WHERE User = '...';
```

## Inštalácia

Predpokladajme, že sú súbory projektu uložené v adresári doctracker (napríklad checkout z gitu). PHP a Composer sú v ceste (path).

### Inštalácia závislostí

V hlavnom adresári projektu spustiť príkaz:

```
composer install
```

### Inicializácia databázy

V hlavnom adresári projektu vytvoriť súbor s názvom `.env`. 

Obsah súboru:

```
APP_ENV=dev
APP_SECRET=007071b808e4fd8beff0108a2588d844
DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name
```

Pre SQLite by mohol vyzerať konf string napr. takto: `DATABASE_URL="sqlite:///%kernel.project_dir%/var/app.db"`

Pri zadávaní príkazov treba byť v koreňovom adresári aplikácie.

Pre vytvorenie databázy v MySql je potrebné zadať nasledujúci príkaz:

```
php bin/console doctrine:database:create
```

Pre vytvorenie databázovej štruktúry zadať príkaz:

```
php bin/console doctrine:schema:update --force
```

Pre načitanie testovacích dát zadať príkaz

```
php bin/console doctrine:fixtures:load
```

### Spustenie aplikácie

Spustiť testovací webový server príkazom:

```
php bin/console server:run
```

Server štandartne počúva na adrese http://127.0.0.1:8000.


## Poznámky