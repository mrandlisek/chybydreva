<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MenuRepository")
 */
class Menu
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Menu", mappedBy="menuParent", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="menu_parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $menuChildren;

    /**
     * @ORM\Column(type="boolean")
     */
    private $defaultMenu;

    /**
     * @ORM\ManyToOne(targetEntity="Menu", inversedBy="menuChildren", cascade={"persist"}, fetch="EXTRA_LAZY")
//     * @ORM\ManyToOne(targetEntity="Menu", inversedBy="menuChildren")
     */
    private $menuParent;


    /**
     * @ORM\OneToMany(targetEntity="Article", mappedBy="menu", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $article;

    public function __construct() {
        $this->menuChildren = new ArrayCollection();
        $this->article = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }
    public function setId($id){
        $this->id = $id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function isDefaultMenu(){
        return $this->defaultMenu;
    }

    /**
     * @return ArrayCollection
     */
    public function getArticle() {
        return $this->article;
    }

    public function setArticle($article): void {
        $this->article = $article;
    }

    public function getMenuParent() {
        return $this->menuParent;
    }

    public function getMenuChildren() {
        return $this->menuChildren;
    }

    public function setMenuParent($menuParent): void {
        $this->menuParent = $menuParent;
    }

    public function setMenuChildren($menuChildren) {
        $this->menuChildren = $menuChildren;
    }

    public function addMenuChildren(Menu $menuChildren) {
        $menuChildren->setMenuChildren($this);
        $this->menuChildren->add($menuChildren);
    }

    public function getDefaultMenu(){
        return $this->defaultMenu;
    }

    public function setDefaultMenu(bool $defaultMenu){
        $this->defaultMenu = $defaultMenu;
    }
}
