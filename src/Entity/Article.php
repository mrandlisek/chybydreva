<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Menu;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 * @ORM\Table(name="article")
 */
class Article
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @var string
     * @Assert\NotBlank()
     */
    private $article;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isHomeArticle;
    /**
     * Pozicia v menu
     * //     * @var ArrayCollection
     * @ORM\ManyToOne(targetEntity="Menu", inversedBy="article", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    private $menu;

    /**
     * @ORM\OneToMany(targetEntity="Article", mappedBy="articleParent", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="menu_parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $subArticle;

    /**
     * @ORM\ManyToOne(targetEntity="Article", inversedBy="subArticle", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    private $articleParent;


    public function __construct() {
        $this->subArticle = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getMenu() {
        return $this->menu;
    }

    public function setMenu($menu): void {
        $this->menu = $menu;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function setId($id): self {
        $this->id = $id;

        return $this;
    }

    public function getTitle(): ?string {
        return $this->title;
    }

    public function setTitle(string $title): self {
        $this->title = $title;

        return $this;
    }

    public function getArticle(): ?string {
        return $this->article;
    }

    public function setArticle(string $article): self {
        $this->article = $article;

        return $this;
    }

    public function getIsHomeArticle(){
        return $this->isHomeArticle;
    }

    public function setIsHomeArticle(bool $isHomeArticle){
        $this->isHomeArticle = $isHomeArticle;
    }


    public function getArticleParent() {
        return $this->articleParent;
    }

    public function getArticleChildren() {
        return $this->subArticle;
    }

    public function setArticleParent($articleParent): void {
        $this->articleParent = $articleParent;
    }

    public function setArticleChildren($subArticle) {
        $this->subArticle = $subArticle;
    }

    public function addArticleChildren(Article $subArticle) {
        $subArticle->setArticleChildren($this);
        $this->subArticle->add($subArticle);
    }
}


