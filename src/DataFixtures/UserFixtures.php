<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEndcoder;
    public function __construct(UserPasswordEncoderInterface $passwordEncoder) {
        $this->passwordEndcoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUuid('mrandlisek');
        $user->setRoles(['ROLE_ADMIN']);
        $user->setPassword($this->passwordEndcoder->encodePassword($user, '0902732036Miro'));
        $manager->persist($user);


        $user2 = new User();
        $user2->setUuid('nmikusova');
        $user2->setRoles(['ROLE_ADMIN']);
        $user2->setPassword($this->passwordEndcoder->encodePassword($user, 'Heslo123'));
        $manager->persist($user2);

        $manager->flush();
    }
}
