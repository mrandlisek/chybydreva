<?php


namespace App\Service;

use App\Entity\Article;
use App\Entity\Menu;
use Doctrine\ORM\EntityManagerInterface;

class MenuService
{
    private $em;
    //TODO Upravit base url na chorobydreva.umb.sk/choroby-a-chyby-dreva/
    private $baseUrl = 'http://chorobydreva.umb.sk/choroby-a-chyby-dreva/';
    private $active = 'active';

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }

    public function generateMenu($parentId, $activeArticleId) {
        $menuItems = $this->em->getRepository(Menu::class)->findBy(['menuParent' => $parentId]);
        $menu = "";
        foreach ($menuItems as $item) {
            if ($parentId != $this->em->getRepository(Menu::class)->findOneBy(['defaultMenu' => 1])->getId()) {
                $menu .= "<li class='nav-item'><p class='nav-link pt-0 pb-0 mb-0'>" . $item->getName() . "</p></li>";

            } else {
                $menu .= "<li class='nav-item'><p class='nav-link pt-1 pb-1 mb-0 mainTopMenu'>" . $item->getName() . "</p>";

            }
            $menu .= "<li class='nav-item'><ul class='nav pl-2'>" . $this->generateLinkForMenuArticle($item->getId(), $activeArticleId)."</ul></li>";
            $menu .= "<li class='nav-item'><ul class='nav pl-2'>" . $this->generateMenu($item->getId(), $activeArticleId) . "</ul></li>";
        }
        return $menu;
    }

    public function generateMenuArticle($parentArticleId, $activeArticleId) {
        $menuArticleItems = $this->em->getRepository(Article::class)->findBy(['articleParent' => $parentArticleId]);
        $menuArticle = "";
        foreach ($menuArticleItems as $item) {
            if ($item->getId() == $activeArticleId) {
                $menuArticle .= "<li class='nav-item'><a class='nav-link pt-0 pb-0 " . $this->active . "' href=" . $this->baseUrl . $item->getId() . ">" . $item->getTitle() . "</a></li>";
            } else {
                $menuArticle .= "<li class='nav-item'><a class='nav-link pt-0 pb-0 mainLink' href=" . $this->baseUrl . $item->getId() . ">" . $item->getTitle() . "</a></li>";
            }
            $menuArticle .= "<li class='nav-item'><ul class='nav pl-2'>" . $this->generateMenuArticle($item->getId(), $activeArticleId) . "</ul></li>";
        }
        return $menuArticle;
    }

    public function generateLinkForMenuArticle($menuId,$activeArticleId){
        $menu = "";
        $articles =$this->em->getRepository(Article::class)->findBy(['menu' => $menuId]);;
        foreach ($articles as $mainArticle) {
            if ($mainArticle->getId() == $activeArticleId) {
                $menu .= "<li class='nav-item'><a class='nav-link pt-0 pb-0 " . $this->active . "' href=" . $this->baseUrl . $mainArticle->getId() . " >" . $mainArticle->getTitle() . "</a></li>";
            } else {
                $menu .= "<li class='nav-item'><a class='nav-link pt-0 pb-0 mainLink' href=" . $this->baseUrl . $mainArticle->getId() . ">" . $mainArticle->getTitle() . "</a></li>";
            }
            $menu .= "<li class='nav-item'><ul class='nav pl-2'>" . $this->generateMenuArticle($mainArticle->getId(), $activeArticleId) . "</ul></li>";
        }
        return $menu;
    }
}