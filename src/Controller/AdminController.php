<?php


namespace App\Controller;


use App\Entity\Article;
use App\Entity\Menu;
use App\Form\ChangePasswordType;
use App\Form\EditArticleType;
use App\Form\EditMenuType;
use App\Model\ChangePassword;
use App\Service\MenuService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AdminController
 * @package App\Controller
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin")
 */
class AdminController extends Controller
{
    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/", name="admin_home")
     */
    public function adminDashboard() {
        $em = $this->getDoctrine()->getManager();
        $articles = $em->getRepository(Article::class)->findAll();
        return $this->render('admin/article.html.twig', array(
            'articles' => $articles
        ));
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/editMenu/{menuId}", defaults={"menuId"=0}, name="edit_menu")
     * @param Request $request
     * @return Response
     */
    public function editMenu(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $menuId = $request->get('menuId');
        if ($menuId == 0) {
            $menu = new Menu();
        } else {
            $menu = $em->find(Menu::class, $menuId);
        }
        $form = $this->createForm(EditMenuType::class, $menu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $menu = $form->getData();
                if ($menu->getMenuParent() == null){
                    $menu->setMenuParent($em->getRepository(Menu::class)->findOneBy(['defaultMenu'=> 1]));
                }
                if ($menu->getDefaultMenu() == null){
                    $menu->setDefaultMenu(0);
                }
                $em->persist($menu);
                $em->flush();
                if ($menuId == 0) {
                    $this->addFlash('info', 'Pridanie menu bolo úspešné');
                } else {
                    $this->addFlash('info', 'Upravenie menu bolo úspešné');
                }
            } catch (\Exception $e) {
                die($e->getMessage());
                $this->addFlash('danger', "Pridanie menu sa nepodarilo {$e->getMessage()}");
            }
            return $this->redirectToRoute('admin_menu');
        }
        return $this->render('admin/editMenu.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Vytvorenie alebo upravenie článku
     *
     * Overenie či má používateľ rolu administrátor
     * @IsGranted("ROLE_ADMIN")
     * Routovanie na adresu admin/editArticle/ kde article id oznacuje zaznam v databáze. Pokial nieje nastavený tak je defaultne nastavený na nulu a vytvára sa nový článok
     * @Route("/editArticle/{articleId}", defaults={"articleId"=0}, name="edit_article")
     * Prijatie requestu ( požiadavky )
     * @param Request $request
     * Vratenie odpovede na požiadavku
     * @return Response
     */
    public function editArticle(Request $request) {
        // Vytvorenie spojenia s databázov
        $em = $this->getDoctrine()->getManager();
        // Načítanie s požiadavky id článku
        $articleId = $request->get('articleId');
        // Kontrola ak je id článku 0 vytvára sa nový ak nie načíta sa s databázy
        if ($articleId == 0) {
            // Vytvorenie entity článok
            $article = new Article();
        } else {
            // Načítanie entity článok
            $article = $em->find(Article::class, $articleId);
        }
        // Vytvorí sa zobrazenie formuláru pre upravovanie článku
        $form = $this->createForm(EditArticleType::class, $article);
        // Prijatie odoslania formuláru
        $form->handleRequest($request);
        // Kontrola či je formulár odoslaný a či má všetky polia validne
        if ($form->isSubmitted() && $form->isValid()) {
            // Pokus o uloženie článku do databázy
            try {
                // Uloženie údajov do entity článok
                $article = $form->getData();
                // Upravenie dát pre uloženie do databázy
                $em->persist($article);
                // Uloženie dát do databázy
                $em->flush();
                if ($articleId == 0) {
                    // Zobrazenie informácie o vytvorení nového článku
                    $this->addFlash('info', 'Pridanie článku bolo úspešné');
                } else {
                    // Zobrazenie informácie o upravení článku
                    $this->addFlash('info', 'Upravenie upravenie bolo úspešné');
                }
                //Odchytenie chyby v prípade ak nejaká vznikla
            } catch (\Exception $e) {
                // Zobrazenie chyby ak vznikla aj s jej popisom
                $this->addFlash('danger', "Pridanie článku sa nepodarilo {$e->getMessage()}");
            }
            // Presmerovanie na domovskú stránku administrácie
            return $this->redirectToRoute('admin_home');
        }
        // Zobrazenie formulára pre používateľa
        return $this->render('admin/editArticle.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/menu", name="admin_menu")
     */
    public function adminMenu() {
        $em = $this->getDoctrine()->getManager();
        $menus = $em->getRepository(Menu::class)->findAll();
        return $this->render('admin/menu.html.twig', array(
            'menus' => $menus
        ));
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/deleteMenu/{menuId}", name="delete_menu")
     * @param Request $request
     * @return Response
     */
    public function deleteMenu(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $menuId = $request->get('menuId');
        $menu = $em->getRepository(Menu::class)->find($menuId);
        try{
            $em->remove($menu);
            $em->flush();
            $this->addFlash('info', "Vymazanie prebehlo úspešne");
        }catch (\Exception $e){
            $this->addFlash('danger', "Vymaznie sa nepodarilo: {$e->getMessage()}");
        }
        return $this->forward("App\Controller\AdminController::adminMenu");
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/deleteArticle/{articleId}", name="delete_article")
     * @param Request $request
     * @return Response
     */
    public function deleteArticle(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $articleId = $request->get('articleId');
        $article = $em->getRepository(Article::class)->find($articleId);
        try{
            $em->remove($article);
            $em->flush();
            $this->addFlash('info', "Vymazanie prebehlo úspešne");
        }catch (\Exception $e){
            $this->addFlash('danger', "Vymaznie sa nepodarilo: {$e->getMessage()}");
        }
        return $this->forward("App\Controller\AdminController::adminDashboard");
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/passwd", name="change_password")
     * @param Request $request
     * @return Response
     */
    public function changePassword(Request $request){
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $changePasswordModel = new ChangePassword();
        $form = $this->createForm(ChangePasswordType::class, $changePasswordModel);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $passwordEncoder = $this->container->get('security.password_encoder');
            $encodedPassword = $passwordEncoder->encodePassword($user, $changePasswordModel->getNewPassword());
            $user->setPassword($encodedPassword);
            $em->persist($user);
            $em->flush();
        }else{
        }

        return $this->render('admin/changePassword.html.twig',array(
            'form' => $form->createView(),
        ));
    }

}