<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Menu;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\MenuService;

/**
 * Class HomeController
 * @package App\Controller
 */
class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     * @param MenuService $menuService
     * @return Response
     */
    public function index(MenuService $menuService) {
        $em = $this->getDoctrine()->getManager();
        $artticle = $em->getRepository(Article::class)->findOneBy(['isHomeArticle' => 1]);
        $idTopMenu = $em->getRepository(Menu::class)->findOneBy(['defaultMenu'=> 1])->getId();
        $menu = $menuService->generateMenu($idTopMenu,'');
        return $this->render('public/home/index.html.twig', [
            'article' => $artticle,
            'menu' => $menu,
            'controller_name' => 'HomeController',
        ]);
    }

    /**
     * @Route("/choroby-a-chyby-dreva/{articleId}", name="article")
     * @param Request $request
     * @param MenuService $menuService
     * @return Response
     */
    public function article(Request $request, MenuService $menuService) {
        $em = $this->getDoctrine()->getManager();
        $articleId = $request->get('articleId');
        $article = $em->getRepository(Article::class)->find($articleId);
        $menu = $menuService->generateMenu($idTopMenu = $em->getRepository(Menu::class)->findOneBy(['defaultMenu'=> 1])->getId(),$articleId);
        return $this->render('public/home/article.html.twig', [
            'article' => $article,
            'menu' => $menu,
            'controller_name' => 'HomeController',
        ]);
    }
}
