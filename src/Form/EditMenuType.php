<?php


namespace App\Form;


use App\Entity\Menu;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class EditMenuType
 * @package App\Form
 * @param FormBuilderInterface $builder
 * @param array $options
 */
class EditMenuType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $menu = $builder->getData();
        $builder
            ->add('id', HiddenType::class)
            ->add('name', TextType::class, array(
                'label_attr' => array(
                    'class' => 'pr-2'
                ),
                'label' => 'Názov položky v menu:'
            ))
            ->add('menuParent', EntityType::class,[
                'label' => 'Nadradene menu:',
                'label_attr' => array(
                    'class' => 'pr-5 pt-2'
                ),
                'attr' => array('class'=> 'mb-xs-2'),
                'expanded'=> false,
                'multiple' => false,
                'required'=> false,
                'class' => Menu::class,
                'choice_label'=> 'name'
            ])
            ->add('submit', SubmitType::class, array(
                'label' => 'Uložiť položku menu',
                'attr'=> array(
                    'class' => 'btn btn-info float-right'
                )
            ))
            ->setMethod('post');
    }

}