<?php


namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class ChangePasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('newPassword', RepeatedType::class, array(
            'type' => PasswordType::class,
            'invalid_message' => 'Heslá sa nezhodujú',
            'options' => array('attr' => array('class' => 'password-field'), 'label_attr' => array('class' => 'pt-2 pr-2')),
            'required' => true,
            'first_options' => array('label' => 'Nové heslo:', 'label_attr'=> array('class'=> 'pr-5')),
            'second_options' => array('label' => 'Zopakovať heslo:'),
        ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Zmeniť heslo',
                'attr' => array(
                    'class' => 'btn btn-info pull-right'
                )
            ))
            ->setMethod('post');;
    }
}