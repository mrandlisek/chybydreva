<?php


namespace App\Form;


use App\Entity\Article;
use App\Entity\Menu;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class EditArticleType
 * @param FormBuilderInterface $builder
 * @param array $options
 * @package App\Form
 */
class EditArticleType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $article = $builder->getData();
        $builder
            ->add('id', HiddenType::class)
            ->add('title', TextType::class, array(
                'label' => 'Nazov',
                'label_attr' => array(
                    'class' => 'pr-2 pb-2'
                ),
                'attr' => array(
                    'class' => 'pr-2'
                )
            ))
            ->add('article', TextareaType::class, array(
                'label' => false,
            ))
            ->add('menu', EntityType::class, [
                'label' => 'Zaradenie v menu',
                'expanded' => false,
                'multiple' => false,
                'required' => false,
                'class' => Menu::class,
                'choice_label' => 'name',
                'label_attr' => array(
                    'class' => 'pr-4 pt-2'
                ),
            ])
            ->add('articleParent', EntityType::class,[
                'label' => 'Podčlánok pre: ',
                'label_attr' => array(
                    'class' => 'pr-5 pt-2'
                ),
                'attr' => array('class'=> 'mb-xs-2'),
                'expanded'=> false,
                'multiple' => false,
                'required'=> false,
                'class' => Article::class,
                'choice_label'=> 'title'
            ])
            ->add('isHomeArticle', CheckboxType::class,[
                'label' => 'Nastaviť tento článok na domovskú obrazovku?',
                'label_attr' => array(
                    'class' => 'pr-2'
                ),
                'required' => false
            ])
            ->add('submit', SubmitType::class, array(
                'label' => 'Uložiť článok',
                'attr' => array(
                    'class' => 'btn btn-info float-right'
                )
            ))
            ->setMethod('post');
    }

}